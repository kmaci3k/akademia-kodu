package pl.akademia.kodu;

import java.util.Scanner;

public class Program {

    public static void silnia() {
        Scanner scanner = new Scanner(System.in);

        int n;
        do {
            System.out.println("Podaj n (0,10):");
            n = scanner.nextInt();
        } while(n < 0 || n > 10);
        System.out.println("Liczymy " + n + "!");

        int iloczyn = 1;
        for(int i = 2; i <= n; ++i) {
            iloczyn = iloczyn * i;
        }
        System.out.println(n + "! = " + iloczyn);
    }

    public static void fibonacci() {
        Scanner scanner = new Scanner(System.in);

        int n;
        do {
            System.out.println("Podaj n (0, 10):");
            n = scanner.nextInt();
        } while(n < 0 || n > 10);

        int a0 = 0;
        if(n > 0) {
            System.out.println("a0 = " + a0);
        }
        int a1 = 1;
        if(n > 1) {
            System.out.println("a1 = " + a1);
        }

        int a_n_1 = a1;
        int a_n_2 = a0;
        for(int i = 2; i < n; ++i) {
            int an = a_n_1 + a_n_2;
            System.out.println("a" + i + " = " + an);
            a_n_2 = a_n_1;
            a_n_1 = an;
        }
    }

    public static void main(String[] args) {
//        silnia();
        fibonacci();
    }
}
