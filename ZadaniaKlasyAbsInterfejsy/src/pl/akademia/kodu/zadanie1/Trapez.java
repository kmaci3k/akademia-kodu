package pl.akademia.kodu.zadanie1;

public class Trapez extends Czworokat {
    private final float bokA;
    private final float bokB;
    private final float bokC;
    private final float bokD;
    private final float wysokosc;

    public Trapez(float bokA,
                  float bokB,
                  float bokC,
                  float bokD,
                  float wysokosc) {
        this.bokA = bokA;
        this.bokB = bokB;
        this.bokC = bokC;
        this.bokD = bokD;
        this.wysokosc = wysokosc;
    }

    @Override
    public float obwod() {
        return bokA + bokB + bokC + bokD;
    }

    @Override
    public float pole() {
        return wysokosc * (bokA + bokB) / 2;
    }

    @Override
    public String toString() {
        return "Trapez{" +
                "bokA=" + bokA +
                ", bokB=" + bokB +
                ", bokC=" + bokC +
                ", bokD=" + bokD +
                ", wysokosc=" + wysokosc +
                ", pole=" + pole() +
                ", obwod=" + obwod() +
                '}';
    }
}
