package pl.akademia.kodu.zadanie1;

public class Kwadrat extends Czworokat {
    private final float dlugoscBoku;

    public Kwadrat(float dlugoscBoku) {
        this.dlugoscBoku = dlugoscBoku;
    }

    @Override
    public float obwod() {
        return 4 * dlugoscBoku;
    }

    @Override
    public float pole() {
//        return Math.pow(dlugoscBoku, 2);
        return dlugoscBoku * dlugoscBoku;
    }

    @Override
    public String toString() {
        return "Kwadrat{" +
                "dlugoscBoku=" + dlugoscBoku + ", " +
                "pole=" + pole() + ", " +
                "obwod=" + obwod() +
                '}';
    }
}
