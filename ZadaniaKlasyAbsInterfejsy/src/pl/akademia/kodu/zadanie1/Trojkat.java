package pl.akademia.kodu.zadanie1;

public class Trojkat implements Figura {
    private final float bokA;
    private final float bokB;
    private final float bokC;
    private final float wysokoscA;

    public Trojkat(float bokA,
                   float bokB,
                   float bokC,
                   float wysokoscA) {
        this.bokA = bokA;
        this.bokB = bokB;
        this.bokC = bokC;
        this.wysokoscA = wysokoscA;
    }

    @Override
    public float obwod() {
        return bokA + bokB + bokC;
    }

    @Override
    public float pole() {
        return bokA * wysokoscA / 2;
    }

    @Override
    public int iloscBokow() {
        return 3;
    }

    @Override
    public String toString() {
        return "Trojkat{" +
                "bokA=" + bokA +
                ", bokB=" + bokB +
                ", bokC=" + bokC +
                ", wysokoscA=" + wysokoscA +
                ", pole=" + pole() +
                ", obwod=" + obwod() +
                '}';
    }
}
