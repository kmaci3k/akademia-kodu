package pl.akademia.kodu.zadanie1;

public interface Figura {
    float obwod();
    float pole();
    int iloscBokow();
}
