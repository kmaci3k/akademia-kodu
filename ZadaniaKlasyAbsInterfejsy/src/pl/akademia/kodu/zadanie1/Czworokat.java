package pl.akademia.kodu.zadanie1;

public abstract class Czworokat implements Figura {

    @Override
    public int iloscBokow() {
        return 4;
    }
}
