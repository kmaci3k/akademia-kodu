package pl.akademia.kodu.zadanie1;

public class Program {

    public static void main(String[] args) {
        Kwadrat kwadrat = new Kwadrat(4);
        System.out.println(kwadrat);

        Prostokat prostokat = new Prostokat(4, 8);
        System.out.println(prostokat);

        Trapez trapez = new Trapez(
                1,
                2,
                3,
                4,
                5);
        System.out.println(trapez);

        Trojkat trojkat = new Trojkat(1, 2, 3, 4);
        System.out.println(trojkat);
    }
}
