package pl.akademia.kodu.zadanie2;

public class Program {

    public static void nastrojIGraj(Instrument instrument) {
        instrument.nastroj();
        instrument.graj();
    }

    public static void main(String[] args) {
        Instrument gitara = new Gitara();
        Instrument klarnet = new Klarnet();

        nastrojIGraj(gitara);
        nastrojIGraj(klarnet);
    }
}
