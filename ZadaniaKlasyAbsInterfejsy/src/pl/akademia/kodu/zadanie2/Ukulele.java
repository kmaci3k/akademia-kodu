package pl.akademia.kodu.zadanie2;

public class Ukulele implements Instrument {
    @Override
    public void graj() {
        System.out.println("Ukulele: graj");
    }

    @Override
    public void nastroj() {
        System.out.println("Ukulele: nastroj");
    }
}
