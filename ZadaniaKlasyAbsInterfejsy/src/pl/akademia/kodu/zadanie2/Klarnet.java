package pl.akademia.kodu.zadanie2;

public class Klarnet implements Instrument {
    @Override
    public void graj() {
        System.out.println("Klarnet: graj");
    }

    @Override
    public void nastroj() {
        System.out.println("Klarnet: nastroj");
    }
}
