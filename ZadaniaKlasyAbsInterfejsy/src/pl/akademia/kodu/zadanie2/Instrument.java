package pl.akademia.kodu.zadanie2;

public interface Instrument {
    void graj();
    void nastroj();
}
