package pl.akademia.kodu.zadanie2;

public class Gitara implements Instrument {
    @Override
    public void graj() {
        System.out.println("Gitara: graj");
    }

    @Override
    public void nastroj() {
        System.out.println("Gitara: nastroj");
    }
}
