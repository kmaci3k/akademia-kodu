package pl.akademia.kodu.zadanie5;

public interface Wiazania {
    void zamontuj();
    void ustaw();
    void zdemontuj();
}
