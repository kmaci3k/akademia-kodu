package pl.akademia.kodu.zadanie5;

public class Program {

    public static void przygotujSprzetNaStok(SprzetNaStok sprzet) {
        sprzet.naostrz();
        sprzet.zamontuj();
        sprzet.ustaw();
    }

    public static void przygotujSprzetNaLodowisko(SprzetNaLodowisko sprzet) {
        sprzet.naostrz();
    }

    public static void main(String[] args) {
        SprzetNaStok narty = new Narty();
        SprzetNaStok snowboard = new Snowboard();
        przygotujSprzetNaStok(narty);
        przygotujSprzetNaStok(snowboard);

        SprzetNaLodowisko lyzwy = new Lyzwy();
        przygotujSprzetNaLodowisko(lyzwy);
    }
}
