package pl.akademia.kodu.zadanie5;

public class Narty extends SprzetNaStok {
    @Override
    public void naostrz() {
        System.out.println("Narty: naostrz");
    }

    @Override
    public void smaruj() {
        System.out.println("Narty: smaruj");
    }

    @Override
    public void regeneracjaSlizgu() {
        System.out.print("Narty: regeneracja slizgu");
    }
}
