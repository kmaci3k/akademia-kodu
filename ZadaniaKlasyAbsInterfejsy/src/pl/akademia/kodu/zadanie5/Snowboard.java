package pl.akademia.kodu.zadanie5;

public class Snowboard extends SprzetNaStok {
    @Override
    public void naostrz() {
        System.out.println("Snowboard: naostrz");
    }

    @Override
    public void smaruj() {
        System.out.println("Snowboard: smaruj");
    }

    @Override
    public void regeneracjaSlizgu() {
        System.out.println("Snowboard: regeneracja slizgu");
    }
}
