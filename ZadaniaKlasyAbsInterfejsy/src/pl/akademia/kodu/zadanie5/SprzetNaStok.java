package pl.akademia.kodu.zadanie5;

public abstract class SprzetNaStok extends SprzetZimowy implements Wiazania {

    public abstract void smaruj();
    public abstract void regeneracjaSlizgu();

    @Override
    public void zamontuj() {
        System.out.println("SprzetNaStok: zamonuj wiazania");
    }

    @Override
    public void ustaw() {
        System.out.println("SprzetNaStok: ustaw wiazania");
    }

    @Override
    public void zdemontuj() {
        System.out.println("SprzetNaStok: zdemontuj wiazania");
    }
}
