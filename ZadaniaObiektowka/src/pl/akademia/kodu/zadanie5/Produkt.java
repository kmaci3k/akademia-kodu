package pl.akademia.kodu.zadanie5;

public class Produkt {
    private String nazwa;
    private String opis;
    private String specyfikacja;

    public Produkt(String nazwa, String opis, String specyfikacja) {
        this.nazwa = nazwa;
        this.opis = opis;
        this.specyfikacja = specyfikacja;
    }

    @Override
    public String toString() {
        return "Produkt{" +
                "nazwa='" + nazwa + '\'' +
                ", opis='" + opis + '\'' +
                ", specyfikacja='" + specyfikacja + '\'' +
                '}';
    }
}
