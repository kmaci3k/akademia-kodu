package pl.akademia.kodu.zadanie5;

import java.time.LocalDate;

public class Gwarancja {

    private final Produkt produkt;
    private final LocalDate dataWaznosci;

    public Gwarancja(Produkt produkt, LocalDate dataWaznosci) {
        this.produkt = produkt;
        this.dataWaznosci = dataWaznosci;
    }

    public boolean jestWazna() {
        LocalDate teraz = LocalDate.now();
        if(dataWaznosci.isAfter(teraz)) {
            return true;
        }
        return false;
    }

    public Produkt getProdukt() {
        return produkt;
    }
}
