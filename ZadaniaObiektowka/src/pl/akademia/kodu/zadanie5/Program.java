package pl.akademia.kodu.zadanie5;

import java.time.LocalDate;

public class Program {

    public static void main(String[] args) {
	    Produkt produkt = new Produkt(
	      "Laptop",
            "Opis laptopa",
            "Specyfikacja laptopa"
        );

        LocalDate dataWaznosciGwarancji = LocalDate
                .of(2018, 03, 19);
        Gwarancja gwarancja = new Gwarancja(
                produkt,
                dataWaznosciGwarancji
        );

        System.out.println(gwarancja.getProdukt());
        System.out.println(
                "Gwarancja jest wazna ? " + gwarancja.jestWazna());

        LocalDate dataNiewaznejGwarancji = LocalDate.of(2016, 03, 19);
        Gwarancja niewaznaGwarancja = new Gwarancja(
                produkt,
                dataNiewaznejGwarancji
        );

        System.out.println(niewaznaGwarancja.getProdukt());
        System.out.println(
                "Gwarancja jest wazna ? " + niewaznaGwarancja.jestWazna());
    }
}
