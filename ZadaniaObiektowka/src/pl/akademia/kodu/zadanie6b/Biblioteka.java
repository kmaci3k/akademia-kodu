package pl.akademia.kodu.zadanie6b;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Biblioteka {

    private List<Ksiazka> ksiazki = new ArrayList<>();

    public Biblioteka() {
        Ksiazka panTadeusz = new Ksiazka(
                "Pan Tadeusz",
                "Adam Mickiewicz"
        );
        Ksiazka ogniemIMieczem = new Ksiazka(
          "Ogniem i Mieczem",
                "Henryk Sienkiewicz"
        );
        Ksiazka potop = new Ksiazka(
                "Potop",
                "Henryk Sienkiewicz"
        );
        ksiazki.add(panTadeusz);
        ksiazki.add(ogniemIMieczem);
        ksiazki.add(potop);
    }

    public void dodajKsiazke() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj tytul ksiazki: ");
        String tytul = scanner.nextLine();

        System.out.println("Podaj autora ksiazki: ");
        String autor = scanner.nextLine();

        Ksiazka nowaKsiazka = new Ksiazka(tytul, autor);
        ksiazki.add(nowaKsiazka);
    }

    public Ksiazka wyszukajPoAutorze(String autor) {
        for(Ksiazka ksiazka : ksiazki) {
            if(ksiazka.getAutor()
                    .toLowerCase()
                    .contains(autor.toLowerCase())) {
                return ksiazka;
            }
        }
        return null;
    }

    public Ksiazka wyszukajPoTytule(String tytul) {
        for(Ksiazka ksiazka : ksiazki) {
            if(ksiazka.getTytul().contains(tytul)) {
                return ksiazka;
            }
        }
        return null;
    }

    public void pokazKsiazki() {
        if(this.ksiazki == null) {
            System.out.println("Biblioteka jest pusta.");
        } else {
            System.out.println(this.ksiazki);
        }
    }
}
