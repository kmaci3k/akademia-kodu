package pl.akademia.kodu.zadanie6b;

import java.util.Scanner;

public class Program {

    public static void pokazMenu() {
        System.out.println("<---- MENU ---->");
        System.out.println("0. KONIEC.");
        System.out.println("1. Pokaz ksiazki.");
        System.out.println("2. Dodaj ksiazke.");
        System.out.println("3. Wyszukaj ksiazke po autorze.");
        System.out.println("4. Wyszukaj ksiazke po tytule.");
    }

    public static void main(String[] args) {
        Biblioteka biblioteka = new Biblioteka();

        Scanner scanner = new Scanner(System.in);
        int opcja;
        do {
            pokazMenu();
            do {
                System.out.println("Wybierz opcje: ");
                opcja = scanner.nextInt();
                scanner.nextLine();
            } while(opcja < 0 || opcja > 4);

            switch (opcja) {
                case 1: {
                    biblioteka.pokazKsiazki();
                    break;
                }
                case 2: {
                    biblioteka.dodajKsiazke();
                    break;
                }
                case 3: {
                    System.out.println("Podaj autora:");
                    String autor = scanner.nextLine();
                    Ksiazka znalezionaKsiazka = biblioteka
                            .wyszukajPoAutorze(autor);
                    if(znalezionaKsiazka != null) {
                        System.out.println(znalezionaKsiazka);
                    } else {
                        System.out.println("Nie znaleziono ksiazki");
                    }
                    break;
                }
                case 4: {
                    System.out.println("Podaj tytul:");
                    String tytul = scanner.nextLine();
                    Ksiazka znalezionaKsiazka = biblioteka
                            .wyszukajPoTytule(tytul);
                    if(znalezionaKsiazka != null) {
                        System.out.println(znalezionaKsiazka);
                    } else {
                        System.out.println("Nie znaleziono ksiazki");
                    }
                    break;
                }
            }

        } while(opcja != 0);
    }
}
