package pl.akademia.kodu.zadanie6a;

import java.util.Scanner;

public class Biblioteka {

    private Ksiazka ksiazka;

    public void dodajKsiazke() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj tytul ksiazki: ");
        String tytul = scanner.nextLine();

        System.out.println("Podaj autora ksiazki: ");
        String autor = scanner.nextLine();

        Ksiazka nowaKsiazka = new Ksiazka(tytul, autor);
        this.ksiazka = nowaKsiazka;
    }

    public void pokazKsiazki() {
        if(this.ksiazka == null) {
            System.out.println("Biblioteka jest pusta.");
        } else {
            System.out.println(this.ksiazka);
        }
    }
}
