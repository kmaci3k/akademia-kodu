package pl.akademia.kodu.zadanie6a;

public class Ksiazka {
    private final String tytul;
    private final String autor;

    public Ksiazka(String tytul, String autor) {
        this.tytul = tytul;
        this.autor = autor;
    }

    @Override
    public String toString() {
        return "Ksiazka{" +
                "tytul='" + tytul + '\'' +
                ", autor='" + autor + '\'' +
                '}';
    }
}
