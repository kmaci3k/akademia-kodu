package pl.akademia.kodu.zadanie6a;

public class Program {
    public static void main(String[] args) {
        Biblioteka biblioteka = new Biblioteka();
        biblioteka.pokazKsiazki();
        biblioteka.dodajKsiazke();
        biblioteka.pokazKsiazki();
    }
}
