package pl.akademia.kodu.zadanie4;


import java.time.Instant;

public class Program {

    public static void zadanie3() {
        Pracownik wlasciciel = new Pracownik(
                "ImieWlasciciela",
                "NazwiskoWlasciciela",
                Instant.parse("2007-12-03T10:15:30.00Z"),
                10
        );

        Pracownik pracownik = new Pracownik(
                "ImiePracownika",
                "NazwiskoPracownika",
                Instant.parse("2007-12-03T10:15:30.00Z")
        );

        Pracownik kierowca = new Pracownik(
                "ImieKierowcy",
                "NazwiskoKierowcy",
                Instant.parse("2007-12-03T10:15:30.00Z"),
                5
        );

        Firma firma = new Firma(wlasciciel, pracownik, kierowca);
        firma.show();
    }

    public static void main(String[] args) {
        zadanie3();
    }
}
