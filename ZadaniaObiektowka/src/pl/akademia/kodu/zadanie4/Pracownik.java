package pl.akademia.kodu.zadanie4;

import java.time.Instant;

public class Pracownik {
    private String imie;
    private String nazwisko;
    private final Instant dataUrodzenia;
//    private final Date dataUrodzenia;
    private int stazPracy;

    public Pracownik(String imie,
                     String nazwisko,
                     Instant dataUrodzenia) {
        this(imie, nazwisko, dataUrodzenia, 0);
    }

    public Pracownik(String imie,
                     String nazwisko,
                     Instant dataUrodzenia,
                     int stazPracy) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataUrodzenia;
        this.stazPracy = stazPracy;
    }

    public void show() {
        String info = String.format("imie: %s, " +
                "nazwisko: %s, " +
                "data urodzenia: %s, " +
                "staz pracy: %d",
                imie,
                nazwisko,
                dataUrodzenia.toString(),
                stazPracy);
        System.out.println(info);
    }
}
