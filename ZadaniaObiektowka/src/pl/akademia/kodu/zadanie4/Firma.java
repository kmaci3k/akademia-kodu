package pl.akademia.kodu.zadanie4;

public class Firma {

    private Pracownik wlasciciel;
    private Pracownik pracownik;
    private Pracownik kierowca;

    public Firma(Pracownik wlasciciel,
                 Pracownik pracownik,
                 Pracownik kierowca) {
        this.wlasciciel = wlasciciel;
        this.pracownik = pracownik;
        this.kierowca = kierowca;
    }

    public void show() {
        wlasciciel.show();
        pracownik.show();
        kierowca.show();
    }
}
