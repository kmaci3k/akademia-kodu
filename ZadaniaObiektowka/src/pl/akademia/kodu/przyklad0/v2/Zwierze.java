package pl.akademia.kodu.przyklad0.v2;

public abstract class Zwierze {

    protected final String imie;

    public Zwierze(String imie) {
        this.imie = imie;
    }

    public abstract void dajGlos();
}
