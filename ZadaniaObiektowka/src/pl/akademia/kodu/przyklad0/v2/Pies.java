package pl.akademia.kodu.przyklad0.v2;

public class Pies extends Zwierze {

    public Pies(String imie) {
        super(imie);
    }

    @Override
    public void dajGlos() {
        System.out.println("Hau...");
    }

    @Override
    public String toString() {
        return String.format("Pies o imieniu: %s.", imie);
    }
}
