package pl.akademia.kodu.przyklad0.v2;

public class PrzykladZwierze {

    public static void zwierzak(Zwierze zwierze) {
        zwierze.dajGlos();
        System.out.println(zwierze.toString());
    }

    public static void main(String[] args) {
        Zwierze kot = new Kot("Kotek");
        Zwierze pies = new Pies("Piesek");

        zwierzak(kot);
        zwierzak(pies);

        kot.dajGlos();
        pies.dajGlos();

        System.out.println(kot.toString());
        System.out.println(pies.toString());
    }
}
