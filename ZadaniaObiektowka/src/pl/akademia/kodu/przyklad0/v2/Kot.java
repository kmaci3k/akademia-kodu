package pl.akademia.kodu.przyklad0.v2;

public class Kot extends Zwierze {

    public Kot(String imie) {
        super(imie);
    }

    @Override
    public void dajGlos() {
        System.out.println("Miau...");
    }

    @Override
    public String toString() {
        return String.format("Kot o imieniu %s.", imie);
    }
}
