package pl.akademia.kodu.przyklad0.v1;

public class Pies implements Zwierze {

    private final String imie;

    public Pies(String imie) {
        this.imie = imie;
    }

    @Override
    public void dajGlos() {
        System.out.println("Hau...");
    }

    @Override
    public String toString() {
        return String.format("Pies o imieniu: %s.", imie);
    }
}
