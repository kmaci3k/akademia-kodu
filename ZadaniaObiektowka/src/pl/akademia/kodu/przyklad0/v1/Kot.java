package pl.akademia.kodu.przyklad0.v1;

public class Kot implements Zwierze {

    private final String imie;

    public Kot(String imie) {
        this.imie = imie;
    }

    @Override
    public void dajGlos() {
        System.out.println("Miau...");
    }

    @Override
    public String toString() {
        return String.format("Kot o imieniu %s.", imie);
    }
}
