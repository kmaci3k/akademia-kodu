package pl.akademia.kodu.zadanie3;

public class Zawodnik {
    private final String imie;
    private final String nazwisko;
    private final String nazwaUzytkownika;
    private int punkty = 0;

    public Zawodnik(String imie,
                    String nazwisko,
                    String nazwaUzytkownika) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.nazwaUzytkownika = nazwaUzytkownika;
    }

    public String imieINazwisko() {
        return imie + " " + nazwisko;
    }

    public void dodajPunkt() {
        ++this.punkty;
    }

    public int getPunkty() {
        return punkty;
    }

    @Override
    public String toString() {
        return "Zawodnik{" +
                "imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                ", nazwaUzytkownika='" + nazwaUzytkownika + '\'' +
                ", punkty=" + punkty +
                '}';
    }
}
