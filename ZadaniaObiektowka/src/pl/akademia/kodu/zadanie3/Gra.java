package pl.akademia.kodu.zadanie3;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Gra {
    private final String nazwa;
    private final List<Zawodnik> zawodnicy = new ArrayList();
    private final Random random = new Random();

    public Gra(String nazwa) {
        this.nazwa = nazwa;
    }

    public void dodajZawodnika() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj imie:");
        String imie = scanner.nextLine();

        System.out.println("Podaj nazwisko:");
        String nazwisko = scanner.nextLine();

        System.out.println("Podaj nazwe uzytkownika:");
        String nazwaUzytkownika = scanner.nextLine();

        Zawodnik nowyZawodnik = new Zawodnik(
                imie,
                nazwisko,
                nazwaUzytkownika
        );
        zawodnicy.add(nowyZawodnik);
    }

    public void pokazZawodnikow() {
        for(Zawodnik zawodnik : zawodnicy) {
            System.out.println(zawodnik);
        }
    }

    public void graj() {
        for(Zawodnik zawodnik : zawodnicy) {
            System.out.println("Aktualny zawodnik:");
            System.out.println(zawodnik);

            int wartosc = random.nextInt(2);
            if(wartosc > 0) {
                System.out.println("Zawodnik zdobywa punkt !!!");
                zawodnik.dodajPunkt();
            } else {
                System.out.println("Zawodnik NIE zdobywa punktu");
            }
        }
    }

    public boolean jestZwyciezca() {
        int wymaganaLiczbaPunktow = 10;
        for(Zawodnik zawodnik : zawodnicy) {
            if(zawodnik.getPunkty() >= wymaganaLiczbaPunktow) {
                return true;
            }
        }
        return false;
    }

    public Zawodnik zwyciezca() {
        int wymaganaLiczbaPunktow = 10;
        for(Zawodnik zawodnik : zawodnicy) {
            if(zawodnik.getPunkty() >= wymaganaLiczbaPunktow) {
                return zawodnik;
            }
        }
        return null;
    }
}
