package pl.akademia.kodu.zadanie3;

public class Program {

    public static void main(String[] args) throws InterruptedException {
        Gra gra = new Gra("Moja Gra");
        gra.dodajZawodnika();
        gra.dodajZawodnika();

        do {
            gra.graj();
            Thread.sleep(1000);
        }while(!gra.jestZwyciezca());

        System.out.println("\nZwyciezca: \n" + gra.zwyciezca());
    }
}
