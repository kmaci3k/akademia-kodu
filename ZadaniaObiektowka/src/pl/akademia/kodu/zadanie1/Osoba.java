package pl.akademia.kodu.zadanie1;

public class Osoba {
    private String name;
    private String surname;
    private int age;
    private Plec gender;

    public Osoba(String name,
                 String surnamne,
                 int age,
                 Plec gender) {
        this.name = name;
        this.surname = surnamne;
        this.age = age;
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Plec getGender() {
        return gender;
    }

    public void setGender(Plec gender) {
        this.gender = gender;
    }
}
