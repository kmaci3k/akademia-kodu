package pl.akademia.kodu.zadanie1;


import java.time.Instant;

public class Program {

    public static void zadanie1() {
        Osoba ja = new Osoba(
                "Maciej",
                "Kowalski",
                29,
                Plec.MEZCZYZNA);

        System.out.println("Name: " + ja.getName());
        System.out.println("Surname: " + ja.getSurname());
        System.out.println("Age: " + ja.getAge());
        System.out.println("Gender: " + ja.getGender());

        ja.setName("Adam");
    }

    public static void main(String[] args) {
        zadanie1();
    }
}
