package pl.akademia.kodu.zadanie2;

public class Samochod {
    private String marka;
    private String model;
    private float cena;

    public Samochod(String marka, String model, float cena) {
        this.marka = marka;
        this.model = model;
        this.cena = cena;
    }

    public void show() {
        String msg = String.format("Marka: %s, model: %s, cena: %f",
                marka,
                model,
                cena);
        System.out.println(msg);
    }
}
