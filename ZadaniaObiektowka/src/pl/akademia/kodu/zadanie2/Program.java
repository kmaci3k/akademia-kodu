package pl.akademia.kodu.zadanie2;


import java.time.Instant;

public class Program {

    public static void zadanie2() {
        Samochod mojSamochod = new Samochod(
                "Fiat",
                "126",
                1000);
        mojSamochod.show();
    }

    public static void main(String[] args) {
        zadanie2();
    }
}
